package hw13;

public class Burger {
    private String bun;
    private String meat;
    private String green;
    private String mayo;
    private String cheese;

    public Burger(String bun, String meat, String green, String mayo, String cheese) {
        this.bun = bun;
        this.meat = meat;
        this.green = green;
        this.mayo = mayo;
        this.cheese = cheese;
        System.out.println("Burger contains " + String.join(", ", bun, meat, green, mayo, cheese) + ".");
    }

    public Burger(String bun, String meat, String green, String cheese) {
        this.bun = bun;
        this.meat = meat;
        this.green = green;
        this.cheese = cheese;
        System.out.println("Burger contains " + String.join(", ", bun, meat, green, cheese) + ".");
    }

    public Burger(String bun, int numOfMeat, String meat, String green, String mayo, String cheese) {
        if(numOfMeat < 2){
            this.meat = 2 + " " + meat;
            System.out.println("You can only make burger with 2 or more meats");
        } else {
            this.meat = numOfMeat + " " + meat;
        }
        this.bun = bun;
        this.green = green;
        this.mayo = mayo;
        this.cheese = cheese;
        System.out.println("Burger contains " + String.join(", ", bun, this.meat, green, mayo, cheese) + ".");
    }
}
