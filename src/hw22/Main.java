package hw22;

public class Main {
    public static void main(String[] args) {
        MyList list = new MyArrayList();
//        System.out.println("Get element with index 0: " + list.get(0));
        System.out.println("Initial size is " + list.size());
        System.out.println("Adding elements at the beginning...");
        list.add("first");
        list.add("second");
        list.add("third");
        list.printAll();
        System.out.println("\n\n===========================================\n");

        System.out.println("Add an element at the begining");
        list.add("BeginElement");
        System.out.println("Add an element at the tail");
        list.add("NewElement", list.size());
//        list.addLast("NewElement");
        list.printAll();
        System.out.println("\n\n===========================================\n");
        System.out.println();


        System.out.println("Add an element at index == 2");
        list.add("Added at index == 2", 2);
        list.printAll();
        System.out.println("\n\n===========================================\n");
        System.out.println();

        String value;
        value = list.remove();
        System.out.println("Remove at the beginning: " + value);
        list.printAll();
        System.out.println("\n\n===========================================\n");
        System.out.println();

        value = list.removeLast();
        System.out.println("Remove at the end: " + value);
        list.printAll();
        System.out.println("\n\n===========================================\n");
        System.out.println();

        value = list.remove(1);
        System.out.println("Remove[index == 1]: " + value);
        list.printAll();

        System.out.println("\n\n===========================================\n");
        System.out.println();
        System.out.println("Final size is " + list.size());
        System.out.println("Get element with index 3: " + list.get(0));
    }
}

