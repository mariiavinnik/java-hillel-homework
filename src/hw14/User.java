package hw14;

public class User {
    private final String name;
    private final int day;
    private final int monthNum;
    private final int year;
    private final String email;
    private final String phoneNum;
    private String surname;
    private double weight;
    private String pressure;
    private int numOfStepsPerDay;
    private int age;

    public User(String name, int day, int monthNum, int year, String email, String phoneNum, String surname, double weight, String pressure, int numOfStepsPerDay) {
        this.name = name;
        this.day = day < 1 ? 1 : day;
        this.monthNum = monthNum < 1 ? 1 : monthNum;
        this.year = year < 1 ? 1 : year;
        this.email = email;
        this.phoneNum = phoneNum;
        this.surname = surname;
        this.weight = weight < 0 ? 0 : weight;
        this.pressure = pressure;
        this.numOfStepsPerDay = numOfStepsPerDay < 0 ? 0 : numOfStepsPerDay;
        this.age = year < 0 ? 0 : 2020 - year;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setWeight(double weight) {
        this.weight = weight < 0 ? 0 : weight;
    }

    public double getWeight() {
        return this.weight;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getPressure() {
        return this.pressure;
    }

    public void setNumOfStepsPerDay(int numOfStepsPerDay) {
        this.numOfStepsPerDay = numOfStepsPerDay < 0 ? 0 : numOfStepsPerDay;
    }

    public int getNumOfStepsPerDay() {
        return this.numOfStepsPerDay;
    }

    public String getName() {
        return this.name;
    }

    public int getDay() {
        return this.day;
    }

    public int getMonthNum() {
        return this.monthNum;
    }

    public int getYear() {
        return this.year;
    }

    public String getEmail() {
        return this.email;
    }
    public int getAge() {
        return this.age;
    }

    public String getPhoneNum() {
        return this.phoneNum;
    }

    public void printAccountInfo() {
        String info = "Full name: " + this.name + " " + this.surname + "\n" +
                "Date of birth: " + this.day + "/" + this.monthNum + "/" + this.year + "\n" +
                "Age: " + this.age + "\n" +
                "Email: " + this.email + "\n" +
                "Phone number: " + this.phoneNum + "\n" +
                "Weight: " + this.weight + "\n" +
                "Pressure: " + this.pressure + "\n" +
                "Steps: " + this.numOfStepsPerDay + "\n";
        System.out.println(info);
    }
}
