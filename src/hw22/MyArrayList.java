package hw22;

public class MyArrayList implements MyList {
    private String[] array;
    private final int defaultLength = 16;
    private int size;

    public MyArrayList() {
        array = new String[defaultLength];
    }

    public MyArrayList(int length) {
        if (length > 0) {
            array = new String[length];
        } else {
            throw new NegativeArraySizeException();
        }
    }

    @Override
    public void add(String value) {
        String[] newArray;
        size++;
        if (size == array.length) {
            newArray = new String[size * 2];
        } else {
            newArray = new String[array.length];
        }

        newArray[0] = value;
        if (size != 1) {
            for (int i = 1; i <= size; i++) {
                newArray[i] = array[i - 1];
            }
        }

        array = newArray;
    }

    @Override
    public void add(String value, int index) {
        if (index == 0 && size == 0) {
            add(value);
            return;
        } else if (index == size) {
            addLast(value);
            return;
        }

        validateIndex(index);

        String[] newArray;
        if (size == array.length) {
            newArray = new String[size * 2];
        } else {
            newArray = new String[array.length];
        }

        for (int i = 0; i < size + 1; i++) {
            if (i < index)
                newArray[i] = array[i];
            else if (i == index)
                newArray[i] = value;
            else
                newArray[i] = array[i - 1];
        }

        array = newArray;
        size++;
    }

    @Override
    public void addLast(String value) {
        if (size == 0) {
            add(value);
            return;
        }

        String[] newArray;
        if (size == array.length) {
            newArray = new String[size + 1];
        } else {
            newArray = new String[array.length];
        }

        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }

        newArray[size] = value;
        array = newArray;
        size++;
    }

    @Override
    public String remove(int index) {
        validateIndex(index);
        String value;
        if (index == 0) {
            value = remove();
        } else if (index == size - 1) {
            value = removeLast();
        } else {
            String[] newArray = new String[array.length];
            value = array[index];

            for (int i = 0, j = 0; i < size; i++, j++) {
                if (i == index) {
                    j++;
                }
                newArray[i] = array[j];
            }

            array = newArray;
            size--;
        }

        return value;
    }

    @Override
    public String removeLast() {
        if (size <= 1) {
            return remove();
        } else {
            String value = array[size - 1];
            array[size - 1] = null;
            size--;

            return value;
        }
    }

    @Override
    public String remove() {
        String value;
        if (size == 0) {
            value = null;
        } else if (size == 1) {
            value = array[0];
            array[0] = null;
        } else {
            String[] newArray = new String[array.length];

            for (int i = 0; i < size; i++) {
                newArray[i] = array[i + 1];
            }

            value = array[0];
            array = newArray;
        }

        size--;
        return value;
    }

    @Override
    public String get(int index) {
        validateIndex(index);

        String result = null;
        for (int i = 0; i < size; i++) {
            if (i == index) {
                result = array[i];
                break;
            }
        }

        return result;
    }

    @Override
    public void validateIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void printAll() {
        for (int i = 0; i < size; i++) {
            System.out.println(array[i]);
        }
    }
}

