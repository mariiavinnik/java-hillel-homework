package hw23;

public class Tree {
    private Node root;

    public void add(int value) {
        root = add(value, root);
    }

    private Node add(int value, Node node) {
        if (node == null) {
            return new Node(value);
        }
        if (value < node.key) {
            node.left = add(value, node.left);
        } else {
            node.right = add(value, node.right);
        }

        return node;
    }

    public Node deleteNode(int value) {
        return deleteNode(root, value);
    }

    public static Node maxElement(Node node) {
        if (node.right == null)
            return node;
        else {
            return maxElement(node.right);
        }
    }

    private Node deleteNode(Node node, int value) {
        if (node == null)
            return null;
        if (node.key > value) {
            node.left = deleteNode(node.left, value);
        } else if (node.key < value) {
            node.right = deleteNode(node.right, value);

        } else {
            if (node.left != null && node.right != null) {
                Node temp = node;
                node.key = temp.left.key;

                Node maxNodeForLeft = maxElement(temp.left);
                maxNodeForLeft.right = temp.right;

                if (node.left.right == null && node.left.left == null) {
                    node.right = temp.right;
                } else {
                    node.right = temp.left.right;
                    node.left = temp.left.left;
                }

                node.left = deleteNode(node.left, maxNodeForLeft.key);
            } else if (node.left != null) {
                node = node.left;
            } else if (node.right != null) {
                node = node.right;
            } else
                node = null;
        }
        return node;
    }

    public Integer find(Integer value) {
        Node result = find(value, root);
        return result == null ? null : result.key;
    }

    private Node find(Integer value, Node node) {
        if (value == null || node == null) {
            return null;
        }
        if (node.key == value) {
            return node;
        }
        if (node.key < value) {
            node = find(value, node.right);
        } else {
            node = find(value, node.left);
        }
        return node;
    }

    public void print() {
        print(root, "");
    }

    private void print(Node node, String prefix) {
        if (node == null) {
            return;
        }
        print(node.left, prefix + "  ");
        System.out.println(prefix + node.key);
        print(node.right, prefix + "  ");
    }

    private static class Node {
        int key;
        Node left;
        Node right;

        public Node(int key) {
            this.key = key;
        }
    }

    public static void main(String[] args) {
        Tree tree = new Tree();
        tree.add(10);
        tree.add(5);
        tree.add(3);
        tree.add(8);
        tree.add(18);
        tree.add(4);
        tree.add(2);
        tree.add(9);
        tree.add(7);
        tree.add(1);

        tree.print();

        System.out.println("\n\n===========================================\n");
        System.out.println();
        tree.deleteNode(5);
        tree.print();

//        System.out.println("\n\n===========================================\n");
//        System.out.println();
//        tree.deleteNode(3);
//        tree.print();

        System.out.println("\n\n===========================================\n");
        System.out.println();
        System.out.println("Find node with value 4: " + tree.find(4));
    }
}
