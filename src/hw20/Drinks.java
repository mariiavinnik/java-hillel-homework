package hw20;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Drinks {
    private final double priceCoffee = 2.40;
    private final double priceTea = 1.40;
    private final double priceLemonade = 2.30;
    private final double priceMojito = 3.00;
    private final double priceMineralWater = 1.50;
    private final double priceCocaCola = 2.00;
    private DrinksMachine currentDrink;
    private static int numberOfDrinks = 0;
    private static double totalAmount = 0;

    public static int getNumberOfDrinks() {
        return numberOfDrinks;
    }

    enum DrinksMachine {
        COFFEE, TEA, LEMONADE, MOJITO, MINERAL_WATER, COCA_COLA;
    }

    Drinks(String drink) {
        this.currentDrink = DrinksMachine.valueOf(drink);
        switch (this.currentDrink) {
            case COFFEE:
                totalAmount += priceCoffee;
                break;
            case TEA:
                totalAmount += priceTea;
                break;
            case LEMONADE:
                totalAmount += priceLemonade;
                break;
            case MOJITO:
                totalAmount += priceMojito;
                break;
            case MINERAL_WATER:
                totalAmount += priceMineralWater;
                break;
            case COCA_COLA:
                totalAmount += priceCocaCola;
                break;
        }
        this.numberOfDrinks++;
    }

    private String makeCoffee() {
        return "Making Coffee";
    }

    private String makeTea() {
        return "Making Tea";
    }

    private String makeLemonade() {
        return "Making Lemonade";
    }

    private String makeMojito() {
        return "Making Mojito";
    }

    private String makeMineralWater() {
        return "Making Mineral water";
    }

    private String makeCocaCola() {
        return "Making Coca-Cola";
    }

    public String makeDrink() {
        String whatToDo = "";
        switch (this.currentDrink) {
            case COFFEE:
                whatToDo = makeCoffee();
                break;
            case TEA:
                whatToDo = makeTea();
                break;
            case LEMONADE:
                whatToDo = makeLemonade();
                break;
            case MOJITO:
                whatToDo = makeMojito();
                break;
            case MINERAL_WATER:
                whatToDo = makeMineralWater();
                break;
            case COCA_COLA:
                whatToDo = makeCocaCola();
                break;
            default:
                whatToDo = "Do nothing";
                break;
        }
        return whatToDo;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("You can drink tea, coffee, lemonade, mojito, mineral_water, coca_cola. Enter 'finish', when you're done.");

        String drinksChoice = "start";
        List<Drinks> drinks = new ArrayList<Drinks>();

        while (true) {
            drinksChoice = scanner.next();
            if (drinksChoice.equals("finish")) {
                break;
            }
            drinks.add(new Drinks(drinksChoice.toUpperCase()));
            System.out.println(drinks.get(drinks.size() - 1).makeDrink());
        }
        System.out.println("You have to pay " + Drinks.totalAmount);
    }
}