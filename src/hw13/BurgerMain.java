package hw13;

public class BurgerMain {
    public static void main(String[] args) {
        Burger[] burgers = new Burger[3];
        burgers[0] = new Burger("bun", "meat", "green", "mayonnaise", "cheese");
        burgers[1] = new Burger("bun", "meat", "green", "cheese");
        burgers[2] = new Burger("bun", 2, "meats", "green", "mayonnaise", "cheese");
    }
}
