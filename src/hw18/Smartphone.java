package hw18;

public interface Smartphone {
    void call();

    void sms();

    void internet();
}
