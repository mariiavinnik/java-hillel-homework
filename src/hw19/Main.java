package hw19;

public class Main {
    public static void main(String[] args) {
        MusicStyles[] bands = new MusicStyles[3];
        bands[0] = new ClassicMusic();
        bands[1] = new RockMusic();
        bands[2] = new PopMusic();

        for (MusicStyles band : bands) {
            band.playMusic();
        }
    }
}
