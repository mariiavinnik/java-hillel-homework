package hw14;

public class UserMain {
    public static void main(String[] args) {
        User[] users = new User[3];
        users[0] = new User("Karina",
                24, 11, 2002,
                "karina3908@gmail.com",
                "+380970591534",
                "Yang",
                65.3,
                "120/60",
                10543
        );
        users[1] = new User("Marina",
                13, 5, 2001,
                "marina21@gmail.com",
                "+380504362174",
                "Prohorchuk",
                48.9,
                "115/60",
                5543
        );
        users[2] = new User("Igor",
                9, 12, 1999,
                "igordoro09@gmail.com",
                "+380672548392",
                "Doroshenko",
                75.5,
                "110/65",
                13213
        );

        users[0].printAccountInfo();
        users[1].printAccountInfo();
        users[2].printAccountInfo();

        users[1].setSurname("Ternova");
        users[1].setWeight(59.5);
        users[1].setNumOfStepsPerDay(3587);
        users[2].setSurname("Grichuk");
        users[2].setNumOfStepsPerDay(10545);

        users[1].printAccountInfo();
        users[2].printAccountInfo();
    }
}
