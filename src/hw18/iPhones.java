package hw18;

public class iPhones implements Smartphone, iOS {
    @Override
    public void call() {
        System.out.println("iPhone is calling...");
    }

    @Override
    public void sms() {
        System.out.println("iPhone is writing sms...");
    }

    @Override
    public void internet() {
        System.out.println("iPhone is checking internet...");
    }
}
