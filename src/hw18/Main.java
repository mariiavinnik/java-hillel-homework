package hw18;

public class Main {
    public static void main(String[] args) {
        Smartphone[] smartphones = new Smartphone[2];
        smartphones[0] = new Android();
        smartphones[1] = new iPhones();

        for (Smartphone smartphone : smartphones) {
            smartphone.call();
            smartphone.sms();
            smartphone.internet();
        }
    }
}
