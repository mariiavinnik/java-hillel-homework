package hw18;

public class Android implements Smartphone, LinuxOs {
    @Override
    public void call() {
        System.out.println("Android is calling...");
    }

    @Override
    public void sms() {
        System.out.println("Android is writing sms...");
    }

    @Override
    public void internet() {
        System.out.println("Android is checking internet...");
    }
}
