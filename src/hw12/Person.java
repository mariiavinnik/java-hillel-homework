package hw12;

public class Person {
    String name;
    String surname;
    String city;
    String phoneNum;

    public Person(String name, String surname, String city, String phoneNum) {
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.phoneNum = phoneNum;
    }

    public String personInfo() {
        String info = "Зателефонувати громадянину " + name + " " + surname + " із міста " + city + " можна за номером " + phoneNum;
        return info;
    }

    public static void main(String[] args) {
        Person person1 = new Person("Марія", "Винник", "Київ", "+380660910825");
        Person person2 = new Person("Олександр", "Пащенко", "Полтава", "+380775618643");
        Person person3 = new Person("Данил", "Назаров", "Харків", "+380674536298");

        System.out.println(person1.personInfo());
        System.out.println(person2.personInfo());
        System.out.println(person3.personInfo());
    }
}