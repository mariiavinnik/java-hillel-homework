package hw22;

public interface MyList {
    void add(String value);

    void add(String value, int index);

    void addLast(String value);

    String remove(int index);

    String removeLast();

    String remove();

    String get(int index);

    void validateIndex(int index);

    int size();

    void printAll();
}
